// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HW_m3GameMode.generated.h"

UCLASS(minimalapi)
class AHW_m3GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHW_m3GameMode();
};



