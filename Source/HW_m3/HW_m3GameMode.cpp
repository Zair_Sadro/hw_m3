// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "HW_m3GameMode.h"
#include "HW_m3HUD.h"
#include "HW_m3Character.h"
#include "UObject/ConstructorHelpers.h"

AHW_m3GameMode::AHW_m3GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AHW_m3HUD::StaticClass();
}
